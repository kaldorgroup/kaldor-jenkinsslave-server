import hudson.model.*

// sets build parameters based on the given map
// only supports StringParameterValue
def setBuildParameters(map) {
    def npl = new ArrayList<StringParameterValue>()
    for (e in map) {
        npl.add(new StringParameterValue(e.key.toString(), e.value.toString()))
    }
    def newPa = null
    def oldPa = manager.build.getAction(ParametersAction.class)
    if (oldPa != null) {
        manager.build.actions.remove(oldPa)
        newPa = oldPa.createUpdated(npl)
    } else {
        newPa = new ParametersAction(npl)
    }
    manager.build.actions.add(newPa)
}

def getTagForCommit(commit) {
  regex = ".*GITTAG\\s+"+commit+":(\\d\\.\\d\\.\\d).*"
  tag = ""
  matcher = manager. getLogMatcher(regex)
  if(matcher?.matches()) {
    tag = matcher.group(1)
  }
  tag
}

currentBuildNumber = manager.build.number
def previous_commit = null
for(i=1; i<=currentBuildNumber; i++) {
    if(manager.setBuildNumber(i)) {
        def commit = manager.build.getEnvironment(manager.listener)['GIT_COMMIT']
        def tag=getTagForCommit(commit);
        setBuildParameters([GIT_TAG: tag.size()==0?"0.0.0":tag]);
        manager.removeSummaries()
        manager.removeBadges()
        if (tag.size()>0) {
            manager.addShortText(tag)
        }
       def description = "<a href='https://bitbucket.org/kaldorgroup/TEMPLATEREPONAME/commits/"+commit+"'>"+commit.substring(0,7)+"</a>"
       if (previous_commit!=null) {
         description += " <a href='https://bitbucket.org/kaldorgroup/TEMPLATEREPONAME/branches/compare/"+previous_commit+".."+commit+"'>diff</a>"
       }
       manager.build.setDescription(description)
       previous_commit = commit
    }
}