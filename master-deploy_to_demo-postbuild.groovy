def env = manager.build.getEnvironment(manager.listener)
def job = env['UPSTREAM_PROJECT']
def description = "<a href='/jenkins/job/"+job+"'>"+job+"</a>"
manager.build.setDescription(description)